package com.example.android.miwok;

import android.widget.ImageView;

/**
 * {@link Word} class that contains a Miwok translation for a default language word.
 */

public class Word {
    private String mMiwokTranslation;
    private String mDefaultTranslation;
    private int mImageResourceID = NO_IMAGE_PROVIDED;
    private int mAudioResourceID;

    private static final int NO_IMAGE_PROVIDED = -1;

    /**
     * @param MiwokTranslation   the miwok translation of the default language word
     * @param DefaultTranslation the default language word
     * @param audioResourceID audoi resource id to find the audio file to use
     */

    public Word(String DefaultTranslation, String MiwokTranslation, int audioResourceID) {
        mMiwokTranslation = MiwokTranslation;
        mDefaultTranslation = DefaultTranslation;
        mAudioResourceID = audioResourceID;


    }

    /**
     * @param DefaultTranslation default translation of the word
     * @param MiwokTranslation   miwok translation of the word
     * @param imageResourceID    image resource id to find the image to use
     * @param audioResourceID audoi resource id to find the audio file to use
     */

    public Word(String DefaultTranslation, String MiwokTranslation, int imageResourceID, int audioResourceID) {
        mMiwokTranslation = MiwokTranslation;
        mDefaultTranslation = DefaultTranslation;
        mImageResourceID = imageResourceID;
        mAudioResourceID = audioResourceID;


    }

    /**
     *
     * @return audio resource id
     */

    public int getAudioResourceID(){return  mAudioResourceID;}
    /**
     * @return image resource id
     */
    public int getImageResourceID() {
        return mImageResourceID;
    }

    /**
     * @return miwok translation of the word
     */

    public String getMiwokTranslation() {
        return mMiwokTranslation;
    }

    /**
     * @return default translation of the word
     */

    public String getDefaultTranslation() {
        return mDefaultTranslation;
    }

    /**
     * Return if there is a valid image associated with the word
     */

    public boolean hasImage() {
        return mImageResourceID != NO_IMAGE_PROVIDED;
    }

    @Override
    public String toString() {
        return "Word{" +
                "mMiwokTranslation='" + mMiwokTranslation + '\'' +
                ", mDefaultTranslation='" + mDefaultTranslation + '\'' +
                ", mImageResourceID=" + mImageResourceID +
                ", mAudioResourceID=" + mAudioResourceID +
                '}';
    }
}
