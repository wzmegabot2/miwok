package com.example.android.miwok;

import android.app.Activity;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class WordAdapter  extends ArrayAdapter<Word> {
    private int mColor;


    public WordAdapter(Activity context, ArrayList<Word> words, int color){
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, words);
        mColor=color;


    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }
        LinearLayout textLinearLayout = (LinearLayout) listItemView.findViewById(R.id.text_LinearLayout);
        int color = ContextCompat.getColor(getContext(),mColor);
        textLinearLayout.setBackgroundColor(color);

        Word words = getItem(position);
        ImageView picImageView = (ImageView) listItemView.findViewById(R.id.image);
        if (words.hasImage()) {


            picImageView.setImageResource(words.getImageResourceID());
            picImageView.setVisibility(View.VISIBLE);
        }
        else
        {
            picImageView.setVisibility(View.GONE);
        }

        TextView miwokTextView= (TextView) listItemView.findViewById(R.id.miwok_translation);
        miwokTextView.setText(words.getMiwokTranslation());


        TextView defaultTextView= (TextView) listItemView.findViewById(R.id.default_translation);
        defaultTextView.setText(words.getDefaultTranslation());

        //mediaPlayer.start();
        return listItemView;
    }


}
